<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


//
// ajout feuille de style
//
function apropos_header_prive($flux){
	$flux = apropos_insert_head($flux);
	return $flux;
}

function apropos_insert_head($flux){
	$flux .= '<link rel="stylesheet" type="text/css" href="'.find_in_path('css/apropos.css').'" />';
	return $flux;
}
